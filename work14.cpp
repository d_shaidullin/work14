﻿#include <iostream>
#include <string>






int main()
{
    /*Homework solution is presented below in one variant + 1 question*/

    // H/w  solution:
    std::cout << "H/w  solution\n\n";
    std::cout << "Enter some string\n";
    std::string Variable;
    std::getline(std::cin, Variable);
    std::cout << "You entered the next string: " << Variable << '\n';
    std::cout << "The length of string '" << Variable << "' is " << Variable.length() << '\n';
    std::cout << "The first symbol of string '" << Variable << "' is " << Variable[0] << '\n';
    std::cout << "The last symbol of string '" << Variable << "' is " << Variable[Variable.length() - 1] << "\n\n";








    // Question:

    // Программа ниже состоит в том, чтобы поделиться с ней именем и возрастом
    // Затем она сравнивает свой возраст с вашим

    std::cout << "Question\n\n";
    // Зададим возраст программы

    int ProgrammAge = 23;

    // Запросим Имя пользователя консоли:)

    std::cout << "Enter your name\n";
    std::string name;
    std::getline(std::cin, name);

    // Создадим булеву переменную со значением true
    // Она поможет нам на случай какой-либо ошибки 
    // Например если пользователь введет возраст буквами

    bool helper = true;

    // Напишем цикл, который поможет пользователю ввести возраст еще раз,
    // если он ввел его некорректно

    // У цикла будет столько итераций, сколько понадобится до тех пор, пока
    // пользователь не введет возраст корректно

    while (helper == true)
    {
        // Обрабатываем искючения связанные с разного рода ошибками при вводе возраста
        try
        {
            // Предположим, что ошибок не возникнет, тогда:

            helper = false;

            // Запросим возраст пользователя

            std::cout << "Enter your age\n";
            std::string age;
            std::getline(std::cin, age);

            // Переведем возраст пользователя в нужный формат
            // чтобы сравнить его с возрастом программы

            int intage = std::stoi(age);
            std::cout << "Glad to see you " << name << "! ";

            // Сравниваем возраст пользователя с возрастом программы 

            if (ProgrammAge > intage)
            {
                std::cout << "I'm " << ProgrammAge - intage << " years older than you\n";
            }
            else if (ProgrammAge == intage)
            {
                std::cout << "Wow, we're the same age\n";
            }
            else
            {
                std::cout << "I'm " << intage - ProgrammAge << " years younger than you\n";
            }
        }

        // Укажем, что надо делать в случае, если возраст пользователя 
        // задан некорректно

        catch (std::invalid_argument e)
        {
            std::cout << "I don't understand. Please, use numbers\n";

            // Переопределим переменную helper, чтобы цикл программа не вышла из цикла,
            // а снова спросила сколько пользователю лет
            helper = true;
        }
    }

    return 0;
}